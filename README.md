# BELAJAR REACT NATIVE PROYEK AL-QUR'AN

ini merupakan aplikasi untuk belajar react native.
sebagai bahan edukasi disini menggunakan api al-qur'an dan axios sebagai penghubungnya.

# CARA CLONE
Pada proses clone pastikan anda terhubung ke internet.
```
git clone https://gitlab.com/kuliah-mobile/belajar-react-native.git
cd belajar-react-native
npm install
npx react native run-android
```

# CARA UPDATE FILE
pada proses dimana terdapat updatan dari master, lakukan perintah berikut:
```
git pull origin master
```