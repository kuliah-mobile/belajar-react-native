import axios from 'axios';

const api = axios.create({
  baseURL: 'https://api.quran.sutanlab.id/',
  timeout: 10000,
});

export default api;
