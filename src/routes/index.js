import React from 'react';
import {View, Text} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {Splash, Home, Juz, Surah, SurahFull} from '../pages';

const Stack = createNativeStackNavigator();

export default function index() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Juz" component={Juz} options={{headerShown: false}} />
      <Stack.Screen
        name="Surah"
        component={Surah}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SurahFull"
        component={SurahFull}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}
