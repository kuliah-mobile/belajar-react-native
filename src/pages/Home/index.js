import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';

export default class index extends Component {
  render() {
    return (
      <View>
        <Text> Home </Text>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Surah')}
          style={styles.btn}>
          <Text>Daftar Surah</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  btn: {
    margin: 5,
    backgroundColor: 'green',
    borderRadius: 5,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
