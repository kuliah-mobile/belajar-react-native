import React, {Component} from 'react';
import {Text, StyleSheet, View, FlatList, TouchableOpacity} from 'react-native';
import api from '../../configs/api';
export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataSurah: null,
    };
  }

  componentDidMount() {
    this.getData();
  }
  // manggil semua daftar surah
  getData = () => {
    api.get('surah').then(hasil => {
      // console.log(hasil.data.data);
      this.setState({
        dataSurah: hasil.data.data,
      });
    });
  };
  // fungsi untuk menampung data per item
  renderRow = ({item, index}) => {
    return (
      <TouchableOpacity
        key={item.number}
        style={styles.list}
        onPress={() =>
          this.props.navigation.navigate('SurahFull', {idSurah: item.number})
        }>
        <Text>{item.name.long}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {dataSurah} = this.state;
    console.log(dataSurah);
    return (
      <View style={styles.container}>
        <Text> Menu Surah </Text>
        {/* komponen untuk menampilkan data */}
        <FlatList data={dataSurah} renderItem={this.renderRow} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  list: {
    backgroundColor: 'lightgray',
    marginBottom: 2,
    borderBottomColor: 'gray',
    padding: 2,
  },
});
