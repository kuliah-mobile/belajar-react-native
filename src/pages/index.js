import Home from './Home';
import Splash from './Splash';
import Juz from './Juz';
import Surah from './Surah';
import SurahFull from './SurahFull';

export {Home, Splash, Juz, Surah, SurahFull};
