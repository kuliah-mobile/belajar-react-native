import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import api from '../../configs/api';

export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: {},
      name: '',
      isLoading: false,
    };
  }

  componentDidMount() {
    this.getData();
  }
  // manggil semua daftar surah
  getData = () => {
    const {idSurah} = this.props.route.params;
    api.get('surah/' + idSurah).then(hasil => {
      console.log(hasil.data.data);
      this.setState({
        isLoading: true,
        content: hasil.data.data.verses,
        name: hasil.data.data.name.long,
      });
    });
  };

  render() {
    const {content, name, isLoading} = this.state;
    return (
      <View>
        <Text> {name} </Text>
        {!isLoading ? (
          <></>
        ) : (
          content.map((data, index) => (
            <View>
              <Text key={index}>{data.text.arab}</Text>
              <Text>{index + 1}</Text>
            </View>
          ))
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
