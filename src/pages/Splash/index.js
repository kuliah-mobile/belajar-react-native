import React, {useEffect} from 'react';
import {Text, StyleSheet, View} from 'react-native';

const index = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 2000);
  });
  return (
    <View style={styles.container}>
      <Text style={styles.text}> Assalamu'alaikum </Text>
    </View>
  );
};

export default index;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'lightgreen',
  },
  text: {
    fontSize: 24,
    color: 'white',
  },
});
